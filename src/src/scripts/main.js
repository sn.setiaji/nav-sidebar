// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

var sidebar = {
    init: function(){
        this.dom()
        this.event()
    },
    dom: function (){
        this.$toggle = $('.menu-list.side-dropdown')
        this.$navToggle = $('.btn-side-toggle')
    },
    event: function(){
        var a = this.$toggle
        var b = this.$navToggle
        var icon = a.find('span')

        var toggle = false;
        var toggle2 = false 

        b.click(function(){
            toggle2 = !toggle2
            if(toggle2 == true){
                $('.side-nav').css('left', '0')
                $(this).find('svg').css('transform', 'rotate(90deg)')
            }else{
                $('.side-nav').css('left', '-200px')
                $(this).find('svg').css('transform', 'rotate(0deg)')
            }
        })

        a.bind('click',function(){
            var x = $(this).attr('sn-target')
            toggle = !toggle

            if (toggle == true){
                a.removeClass('active')
                $(x).addClass('active')
                $(this).find('span').removeClass('unrotated').addClass('rotated')
            }else{
                $(x).removeClass('active')
                $(this).find('span').removeClass('rotated').addClass('unrotated')                
            }
        })
    }
}

sidebar.init()